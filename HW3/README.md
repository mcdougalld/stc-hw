# README #

This is homework 2 (third-party libraries) for STC Fall 2017.  This
homework is due on the 2nd of November 2017 at 11:59pm.

### Instructions ###

Please complete this assignment on stampede2.tacc.utexas.edu using a command
line editor (vim, emacs, nano) and command line tools.  You will need to clone
your bitbucket repo on stampede2, and set up a remote to point to Anne's
homework repo.

This homework requires you to write source code (C/C++/Fortran and LaTeX).
Please use version control as you do your homework.

Do not hesitate to ask for help and also post to piazza.

1. The file `integration.C` contains a C++ program.  There is also a `Makefile`
   provided.  Compile the code.  A good `Makefile` is one with a `check` target
   that runs a suite of tests.  Run `make check`.  Do all the tests pass?  One
   of them should fail (for now).

2. There is a function called `f` in the code.  The function `integrate`
   implements an integration routine to integrate `f` between 0.0 and 0.5.  It
   is not a clever integration procedure.  Implement a better integration
   procedure using the third-party library GSL.  Use GSL's QAG adaptive
   integration routines:
   https://www.gnu.org/software/gsl/manual/html_node/QAG-adaptive-integration.html#QAG-adaptive-integration.
   When creating a workspace for GSL, use 3 intervals.  When calling
   `gsl_integration_qag`, use zero for the absolute error parameter.  For the
   relative error parameter use the `TOL` macro.  For `limit` parameter, use the
   number of levels you allocated for in the workspace.  For the `key` parameter
   to use `GSL_INTEG_GAUSS61`.  Feel free to borrow heavily from a GSL example
   in the documentation to help you.  Put your better integration routine inside
   the `integrate2` function.  You'll need to modify the makefile to compile and
   link the updated source with GSL on stampede2.  The test suite will run your
   integration routine and check the result is correct.  Commit the updated
   source file only if all the tests pass.

3. Profile the `integration` executable using `gprof`.  To do this you'll need to
   modify the makefile to use the the appropriate compiler/linker flag.  Store
   the profile in a file called `profile.txt`.  Commit this file to your repo.
   Look at the `profile.txt` file and determine which of the integration
   routines, `integrate` or `integrate2`, is faster.  Record your observation in
   the accompanying `hw3.txt` file.  Commit it.

4. Given your observation in question 3, dicsuss whether you think that it is a
   good idea or a bad idea to use third-party libraries for integration.  Write
   your answer in the `hw3.txt` file and commit it.

5. Look at the function `f`.  This is the function we're integrating.  For this
   function, discuss whether you think third-party libraries are an appropriate
   choice for integration routines.  Record your answer in `hw3.txt` and commit
   it.

Grading criteria:
i.  Frequent use of git, and commits are made only when tests pass.
ii.  Correct answers/observations.
iii.  Reasoning for dicussion questions is written clearly.

### Need Help? ###

Questions? Don't forget to use our piazza message
board! https://piazza.com/utexas/fall2017/sds335394 and also you can contact UV
Yadav udaivir.yadav@utmail.utexas.edu, Anne Bowen adb@tacc.utexas.edu or Damon
McDougall dmcdougall@tacc.utexas.edu
