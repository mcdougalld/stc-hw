import h5py
import numpy as np
import matplotlib
matplotlib.use('pdf')
from matplotlib import pyplot as plt

sol_file = h5py.File('sol.h5', 'r')
data = sol_file['/sol']

data_with_bc = np.zeros(data.shape[0] + 2)
data_with_bc[1:data_with_bc.shape[0] - 1] = data
x = np.linspace(0, 1, num=data_with_bc.shape[0], endpoint=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x, data_with_bc)
ax.set_ylabel('u')
ax.set_xlabel('x')
ax.set_xlim(0, 1)
ax.grid(True)
fig.savefig('sol.pdf')
